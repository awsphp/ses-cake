<?php
include_once(ROOT.DS . 'vendors' . DS . 'aws'.DS.'autoload.php');

use Aws\Ses\SesClient;  
use Aws\Exception\AwsException; 

class SesComponent extends Component {
    protected $controller = null;
    public $ses;
    public $emailViewPath = '/Emails';
    public $emailLayouts = 'Emails';
    public $htmlMessage;
    public $from = 'Email wire <info@example.com>';
    public $to;
    public $replyto="noreplay@example.com";
    public $subject;
	public $region='us-east-1';
    public $textMessage;
    public $credentials=[];

    public function beforeFilter() {
        parent::beforeFilter();
    }
    public function initialize(Controller $controller) {
        $this->controller = & $controller;
    }
    
    
    
    public function _aws_ses(){ 
            try {
            $config=[ 
                    'version' => '2010-12-01',
                    'region' => $this->region,
                    'credentials' => $this->credentials,
                ];

            $this->ses = new Aws\Ses\SesClient($config);
            $char_set = 'UTF-8'; 
            $recipientEmails=explode(',', $this->to);
            $requestMail=[
                'Destination' => [
                    'ToAddresses' => $recipientEmails,
                ],
                'ReplyToAddresses' => [$this->replyto],
                'Source' => $this->from,
                'Message' => [
                    'Body' => [
                        'Html' => [
                            'Charset' => $char_set,
                            'Data' => $this->htmlMessage,
                        ],
                        // 'Text' => [
                        //     'Charset' => $char_set,
                        //     'Data' => $plaintext_body,
                        // ],
                    ],
                    'Subject' => [
                        'Charset' => $char_set,
                        'Data' => $this->subject,
                    ],
                ], 
            ]; 
            // pr($requestMail);die;
            $response = $this->ses->sendEmail($requestMail);
            $messageId = $response->get('MessageId');
            if (empty($messageId)){
                echo 'Message not sent.';
                return false;
            }
        return true;
        } catch (Exception $e) {
            // output error message if fails
            // echo $e->getMessage();
            echo "Note: <b>From and to both mail shoud be verified and set credentials</b> <br/> <br/> <br/>".$e->getMessage();
            die;
            return false;
        }
        return true;
    } 
    public function _getHTMLBodyFromEmailViews($view){
       $currentLayout = $this->controller->layout;
       $currentAction = $this->controller->action;
       $currentView = $this->controller->view;
       $currentOutput = $this->controller->output;

       ob_start();
       $this->controller->output = null;

       $viewPath = $this->emailViewPath . DS . 'html' . DS . $view;
       $layoutPath = $this->emailLayouts . DS . 'html' . DS . 'default';

       $bodyHtml = $this->controller->render($viewPath, $layoutPath);

       ob_end_clean();

       $this->controller->layout = $currentLayout;
       $this->controller->action = $currentAction;
       $this->controller->view = $currentView;
       $this->controller->output = $currentOutput;

       return $bodyHtml;
    } 
}