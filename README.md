# AWS-SES CakePHP
## Requirment
	1. Composer OR [#INSTALL](https://getcomposer.org/)
	2. PHP 7.x OR 8.x

## Configration
	[#SES] Setup SES configration at AWS.
			Create User for SES and save the key and secreat key 
			Verify the email to send (For testing only)
			OR 
			You can flow the step from 
			https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/ses-examples.html

	[#AWS Took] Download AWS SDK (https://aws.amazon.com/sdk-for-php/) and put in vendors/aws folder.
			OR
		Update SDK dependancies in vendors/aws : composer update

	# configrations
	$this->Ses->credentials=[
			'key'    => 'AXXXXXXXXXXXDOA',
			'secret' => '5aXXXXXXXXXXXXXXXXXXXXXXX+/XXX',
		];
	$this->Ses->region='us-east-1';
    $this->Ses->from ="SITE Name <from_email@gmail.com>";
    $this->Ses->to ="to_email@gmail.com";
    $this->Ses->subject ="Email Subject here!";
    $this->Ses->replayto="noreply@gmail.com";
    $this->Ses->htmlMessage="Type Your Message";
	$this->Ses->_aws_ses();

## Get Support!

[#AWS](https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/ses-examples.html)
[#SES](https://github.com/awsdocs/aws-doc-sdk-examples/tree/main/php/example_code/ses)

 